package com.globant.pokeglow.util;


import com.globant.pokeglow.R;

import android.content.Context;
import android.content.SharedPreferences;

import static com.globant.pokeglow.util.Constants.EMPTY;


public class GloberSharedPreferences {
    private final Context context;
    private final SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public GloberSharedPreferences(Context context) {
        this.context = context;
        String fileName = context.getString(R.string.preference_file_glober);
        preferences = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
    }

    public GloberSharedPreferences startEditSession() {
        editor = preferences.edit();
        return this;
    }

    public GloberSharedPreferences putEmail(final String email) {
        editor.putString(context.getString(R.string.email), email);
        return this;
    }

    public GloberSharedPreferences putGloberName(final String name) {
        editor.putString(context.getString(R.string.name), name);
        return this;
    }

    public GloberSharedPreferences putPosition(final String position) {
        editor.putString(context.getString(R.string.position), position);
        return this;
    }

    public GloberSharedPreferences putStudio(final String studio) {
        editor.putString(context.getString(R.string.studio), studio);
        return this;
    }

    public void save() {
        editor.commit();
    }

    public String getEmail() {
        return preferences.getString(context.getString(R.string.email), EMPTY);
    }

    public String getGloberName() {
        return preferences.getString(context.getString(R.string.name), EMPTY);
    }

    public String getPosition() {
        return preferences.getString(context.getString(R.string.position), EMPTY);
    }

    public String getStudio() {
        return preferences.getString(context.getString(R.string.studio), EMPTY);
    }

    public void reset() {
        startEditSession();
        editor.clear();
        save();
    }
}
