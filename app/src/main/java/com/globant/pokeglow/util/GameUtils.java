package com.globant.pokeglow.util;

import com.globant.pokeglow.game.engine.SceneActor;
import com.globant.pokeglow.ui.GameActivity;
import com.globant.pokeglow.ui.ImageActivity;
import com.globant.pokeglow.ui.LoginActivity;
import com.globant.pokeglow.ui.ProfileActivity;

import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class GameUtils {

    public static int screenWidth;
    public static int screenHeight;
    public static Context currentContext;

    public static Bitmap getBitmapFromAsset(String filePath) {
        AssetManager assetManager = currentContext.getAssets();

        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            // handle exception
        }

        return bitmap;
    }

    public static Bitmap getCurrentFrameActor(Bitmap frames, SceneActor.DIRECTION direction, int step) {
        int coordinateX = step * 64;
        int coordinateY = direction.getValue() * 64;
        return Bitmap.createBitmap(frames, coordinateX, coordinateY, 64, 64);
    }

    public static JSONObject getJsonInfoFromFile(String filePath) {
        AssetManager assetManager = currentContext.getAssets();

        ;
        try {
            InputStream istr = assetManager.open(filePath);
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(istr, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null)
                responseStrBuilder.append(inputStr);

            return new JSONObject(responseStrBuilder.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void startImageActivity(String name) {
        Intent imageActivity = new Intent(currentContext, ImageActivity.class);
        imageActivity.putExtra("image", name);
        imageActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        currentContext.startActivity(imageActivity);
    }

    public static void startProfileActivity() {
        Intent imageActivity = new Intent(currentContext, ProfileActivity.class);
        imageActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        currentContext.startActivity(imageActivity);
    }

    public static void reStartApp(){
        GameActivity.finishApp();
        Intent restart = new Intent(currentContext, LoginActivity.class);
        restart.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        currentContext.startActivity(restart);
    }

}
