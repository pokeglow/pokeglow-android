package com.globant.pokeglow.ui;

import com.globant.pokeglow.util.GameUtils;
import com.globant.pokeglow.R;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.ImageView;

public class ImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        String name = getIntent().getStringExtra("image");
        if(name != null && !TextUtils.isEmpty(name))
        getSupportActionBar().setTitle(name);
        Bitmap bitmap = GameUtils.getBitmapFromAsset(name+".jpg");
        ImageView image = (ImageView)findViewById(R.id.imageView);
        image.setImageBitmap(bitmap);
    }
}
