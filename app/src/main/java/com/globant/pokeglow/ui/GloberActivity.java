package com.globant.pokeglow.ui;

import com.globant.pokeglow.R;
import com.globant.pokeglow.util.GloberSharedPreferences;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class GloberActivity extends AppCompatActivity {
    private Spinner mStudioSpinner;
    private Spinner mPositionSpinner;

    private GloberSharedPreferences globerPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glober);

        mPositionSpinner = (Spinner) findViewById(R.id.position_spinner);
        mStudioSpinner = (Spinner) findViewById(R.id.studio_spinner);
        globerPreferences = new GloberSharedPreferences(this);

        Button continueButton = (Button) findViewById(R.id.continueButton);
        final EditText nameEditText = (EditText) findViewById(R.id.nameEditText);

        populateStudioSpinner();
        populatePositionSpinner();

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                globerPreferences.
                    startEditSession().
                    putGloberName(nameEditText.getText().toString()).
                    putPosition(mPositionSpinner.getSelectedItem().toString()).
                    putStudio(mStudioSpinner.getSelectedItem().toString()).
                    save();
                finish();
                startGameActivity();
            }
        });
    }

    private void startGameActivity() {
        Intent intent = new Intent(GloberActivity.this, GameActivity.class);
        startActivity(intent);
    }

    private void populatePositionSpinner() {
        ArrayAdapter<CharSequence> adapter =
            createArrayAdapterFromResource(R.array.tech_positions);
        mPositionSpinner.setAdapter(adapter);
    }

    private void populateStudioSpinner() {
        ArrayAdapter<CharSequence> adapter =
            createArrayAdapterFromResource(R.array.studios);
        mStudioSpinner.setAdapter(adapter);
    }

    private ArrayAdapter<CharSequence> createArrayAdapterFromResource(final int resource) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.
                createFromResource(this, resource, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return adapter;
    }
}
