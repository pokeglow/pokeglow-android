package com.globant.pokeglow.ui;


import com.globant.pokeglow.game.engine.GameScene;
import com.globant.pokeglow.util.GameUtils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameCustomView extends SurfaceView implements SurfaceHolder.Callback {

    private Thread mThread;
    private boolean isPaused = false;
    private boolean isFinished = false;
    private GameScene scene;

    private long framesPerSecond = 30;
    private long skipTick = 1000/framesPerSecond;


    public GameCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        GameUtils.screenHeight = getHeight();
        GameUtils.screenWidth = getWidth();
        if(mThread == null) {
            scene = new GameScene();
            scene.loadScene();
            mThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    loopGame();
                }
            });
            mThread.start();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        GameUtils.screenHeight = getHeight();
        GameUtils.screenWidth = getWidth();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    public void touched(MotionEvent touch) {
        scene.sendTouchEvent((int)touch.getX(), (int)touch.getY(), touch.getAction());
    }

    public void onPause() {
        isPaused = true;
    }

    public void onResume() {
        isPaused = false;
    }

    public void drawScene(Canvas canvas) {
        scene.draw(canvas);
    }

    private void loopGame() {
        while (!isFinished) {
            long startTime = System.currentTimeMillis();
            if (isPaused){
                try {
                    Thread.sleep(skipTick);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                continue;
            }

            scene.updateScene(1);

            Canvas canvas = getHolder().lockCanvas();

            synchronized (getHolder()) {
                if (!getHolder().getSurface().isValid()) {
                    continue;
                }
                /** Start editing pixels in this surface.*/

                drawScene(canvas);
                // End of painting to canvas. system will paint with this canvas,to the surface.
                getHolder().unlockCanvasAndPost(canvas);
            }
            long currentEnlapsedTime = System.currentTimeMillis() - startTime;
            long sleptTime = skipTick - currentEnlapsedTime;
            if(sleptTime > 0) {
                try {
                    Thread.sleep(sleptTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
