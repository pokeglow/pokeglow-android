package com.globant.pokeglow.ui;

import com.globant.pokeglow.util.GameUtils;
import com.globant.pokeglow.R;
import com.globant.pokeglow.util.GloberSharedPreferences;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class ProfileActivity extends AppCompatActivity {
    private Button mLogoutButton;
    private GloberSharedPreferences globerPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        globerPreferences = new GloberSharedPreferences(this);

        mLogoutButton = (Button) findViewById(R.id.logoutButton);
        TextView emailText = (TextView) findViewById(R.id.emailTextView);
        TextView studioText = (TextView) findViewById(R.id.studioTextView);
        TextView positionText = (TextView) findViewById(R.id.positionTextView);
        TextView globerNameText = (TextView) findViewById(R.id.globerNameTextView);

        emailText.setText(String.format("Email: %s", globerPreferences.getEmail()));
        studioText.setText(String.format("Studio: %s", globerPreferences.getStudio()));
        positionText.setText(String.format("Position: %s", globerPreferences.getPosition()));
        globerNameText.setText(String.format("Glober: %s", globerPreferences.getGloberName()));

        mLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
    }

    public void logout() {
        finish();
        GameUtils.reStartApp();
    }
}
