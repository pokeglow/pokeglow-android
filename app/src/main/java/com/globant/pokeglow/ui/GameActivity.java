package com.globant.pokeglow.ui;

import com.globant.pokeglow.util.GameUtils;
import com.globant.pokeglow.R;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.WindowManager;

public class GameActivity extends AppCompatActivity {

    GameCustomView gameView;
    public static GameActivity mthis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game);
        gameView = (GameCustomView)findViewById(R.id.gameView);
        mthis = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameUtils.currentContext = getApplicationContext();
        gameView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameView.onPause();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gameView.touched(event);
        return super.onTouchEvent(event);
    }

    public static void finishApp() {
        if(mthis != null) {
            mthis.finish();
        }
    }
}
