package com.globant.pokeglow.game.engine;

import com.globant.pokeglow.util.GameUtils;


public class ProfileButton extends SceneButton {

    @Override
    public void initInScene(int x, int y) {

    }

    @Override
    public void load() {
        posX = GameUtils.screenWidth - 96;
        posY = 0;
        currentBitmap = GameUtils.getBitmapFromAsset("profile.png");
    }

    @Override
    public boolean isInCoordinate(int x, int y) {
        return x>posX && y> posY && y< 96;
    }

    @Override
    public void update(long step) {

    }

    @Override
    public void setStatus(STATUS status) {
        super.setStatus(status);
        if(status == STATUS.RELEASED) {
            GameUtils.startProfileActivity();
        }
    }
}
