package com.globant.pokeglow.game.engine;

import com.globant.pokeglow.util.GameUtils;

import android.graphics.Bitmap;

import java.util.HashSet;
import java.util.Set;

public class PlayerActor extends SceneActor {

    Bitmap frames;
    int currenFrame;
    int currentTimeToUpdate = 0;
    DIRECTION currentDirection;
    int velocity = 3;
    int framesToChange = 6;
    int frameCounter = 0;
    boolean isMoving = false;
    private boolean isColliding = false;

    private Set<PlayerCollisionListener> movementListeners;

    public PlayerActor() {
        currentDirection = DIRECTION.DOWN;
        posX = 200;
        posY = 200;

        movementListeners = new HashSet<>();
    }

    @Override
    public void initInScene(int x, int y) {
        posX = x;
        posY = y;
    }

    @Override
    public void step(long frameStep) {
        frameCounter += frameStep;
        if(isMoving) {
            switch ( currentDirection ) {
                case DOWN:
                    posY += velocity;
                    break;
                case LEFT:
                    posX -= velocity;
                    break;
                case RIGHT:
                    posX += velocity;
                    break;
                case UP:
                    posY -= velocity;
                    break;
            }
        }

        notifyMovementListeners();
        if(isColliding) {
            switch ( currentDirection ) {
                case DOWN:
                    posY -= velocity;
                    break;
                case LEFT:
                    posX += velocity;
                    break;
                case RIGHT:
                    posX -= velocity;
                    break;
                case UP:
                    posY += velocity;
                    break;
            }
        }
        if(posY + 64> GameUtils.screenHeight) {
            posY -= velocity;
        }
        if(posY < 0) {
            posY += velocity;
        }
        if(posX + 64> GameUtils.screenWidth) {
            posX -= velocity;
        }
        if(posX < 0) {
            posX += velocity;
        }


        boolean mustChangeFrame = frameCounter > framesToChange;
        if (mustChangeFrame && isMoving) {
            frameCounter = 0;
            currenFrame++;
            if (currenFrame > 3) {
                currenFrame = 0;
            }
            currentBitmap = GameUtils.getCurrentFrameActor(frames, currentDirection, currenFrame);
        } else if (mustChangeFrame && !isMoving && currenFrame != 0) {
            currenFrame = 0;
            currentBitmap = GameUtils.getCurrentFrameActor(frames, currentDirection, currenFrame);
        }
    }

    @Override
    public void load() {
        frames = GameUtils.getBitmapFromAsset("boy_brownhair.png");
        currentBitmap = GameUtils.getCurrentFrameActor(frames, currentDirection, currentTimeToUpdate);
    }

    @Override
    public boolean isInCoordinate(int x, int y) {
        return posX<x && x<(posX+64) && posY <y && y<(posY+64);
    }

    @Override
    public void touchEvent(int type) {
        //do nothing
    }

    public void control(DIRECTION newDirection, boolean moving) {
        currentDirection = newDirection;
        isMoving = moving;
    }

    public boolean isColliding(){ return isColliding; }

    public void setColliding( boolean isColliding ){
        this.isColliding = isColliding;
    }

    public boolean addMovementListener( PlayerCollisionListener listener ){
        return movementListeners.add( listener );
    }

    private void notifyMovementListeners(){
        for( PlayerCollisionListener listener : movementListeners ){
            if( listener.playerCollisionedWhileMoving( this ) ){
                this.setColliding( true );
                return;
            }
            this.setColliding( false );
        }

    }
}
