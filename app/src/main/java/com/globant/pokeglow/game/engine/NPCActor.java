package com.globant.pokeglow.game.engine;

import com.globant.pokeglow.util.GameUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.Rect;

import java.util.ArrayList;

public class NPCActor extends SceneActor implements PlayerCollisionListener {
    String name;
    String NPCName;
    String imageFrames;
    String presentation;
    boolean isPresentationGiven = false;
    ArrayList<NPCQuestion> questions;

    public NPCActor(String NpcName, JSONObject info, Coordinates coordinates) {
        this( NpcName, info );
        this.posX = coordinates.getX();
        this.posY = coordinates.getY();
    }

    public NPCActor(String NpcName, JSONObject info) {
        NPCName = NpcName;
        try {
            imageFrames = info.getString("image");
            name = info.getString("name");
            presentation = info.getString("presentation");
            if (info.has("questions")) {
                questions = new ArrayList<>();
                JSONArray array = info.getJSONArray("questions");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    NPCQuestion question = new NPCQuestion();
                    question.question = obj.getString("call_to_action");
                    question.type = obj.getString("type");
                    question.answer = obj.getString("content");
                    questions.add(question);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initInScene(int x, int y) {

    }

    @Override
    public void step(long frameStep) {

    }

    @Override
    public void load() {
        try {
            Bitmap allframes = GameUtils.getBitmapFromAsset(imageFrames);
            currentBitmap = GameUtils.getCurrentFrameActor(allframes, DIRECTION.DOWN, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isInCoordinate(int x, int y) {
        return posX<x && x<(posX+64) && posY <y && y<(posY+64);
    }

    @Override
    public void touchEvent(int type) {
        if(type == 1) {
            GameUtils.startImageActivity(NPCName);
        }
    }

    public boolean collideWith(PlayerActor player) {
        Rect a = new Rect(posX, posY, posX + 64, posY + 64);
        Rect b = new Rect(player.posX, player.posY, player.posX + 64, player.posY + 64);
        return Rect.intersects(a,b);
    }

    @Override
    public boolean playerCollisionedWhileMoving(PlayerActor player) {
        return collideWith( player );
    }

    public String getNextTextToShow() {
        if(!isPresentationGiven) {
            isPresentationGiven = true;
            return presentation;
        } else {
            return "...";//TODO retur more info
        }
    }

    public boolean isNearFrom(int x, int y,int distance) {
        return (x > (posX - distance) && x < (posX + distance)
                && y > (posY - distance) && y < (posY + distance));
    }
}
