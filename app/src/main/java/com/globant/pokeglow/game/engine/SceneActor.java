package com.globant.pokeglow.game.engine;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public abstract class SceneActor {

    public enum DIRECTION {
        DOWN(0),
        LEFT(1),
        RIGHT(2),
        UP(3);

        int intValue;
        DIRECTION(int value) {
            intValue = value;
        }
        public int getValue() {return intValue;}
    }

    int posX;
    int posY;
    protected Bitmap currentBitmap;

    public abstract void initInScene(int x, int y);

    public void paint(Canvas canvas, Paint paint) {
        if(currentBitmap != null) {
            paint.setFilterBitmap(true);
            canvas.drawBitmap(currentBitmap, posX, posY, paint);
        }
    }

    public void move(int deltaX, int deltaY) {
        posX += deltaX;
        posY += deltaY;
    }

    public abstract void step(long frameStep);


    public abstract void load();

    public abstract boolean isInCoordinate(int x, int y);

    public abstract void touchEvent(int type);

}
