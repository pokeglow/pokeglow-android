package com.globant.pokeglow.game.engine;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public abstract class SceneButton {

    public enum STATUS {
        PRESSED(1),
        RELEASED(0);

        int intValue;
        STATUS(int value) {
            intValue = value;
        }
        public int getValue() {return intValue;}
    }

    int posX;
    int posY;
    protected Bitmap currentBitmap;

    STATUS mStatus;

    public abstract void initInScene(int x, int y);

    public void paint(Canvas canvas, Paint paint) {
        if(currentBitmap != null) {
            paint.setFilterBitmap(true);
            canvas.drawBitmap(currentBitmap, posX, posY, paint);
        }
    }



    public abstract void load();

    public abstract boolean isInCoordinate(int x, int y);

    public void setStatus(STATUS status) {
        mStatus = status;
    }

    public abstract void update(long step);
}
