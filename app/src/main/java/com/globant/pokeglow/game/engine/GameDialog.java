package com.globant.pokeglow.game.engine;

import com.globant.pokeglow.util.GameUtils;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;


public class GameDialog {

    String currentTextToShow = "";
    String currentTitle;

    String globalText = "";

    int posX;
    int posY;
    int endX;
    int endY;
    int border = 2;
    int fontSize = 48;
    int desiredWidth;
    int lettersPerRow;
    boolean isActive = false;


    Paint textPaint;

    public GameDialog() {
        posX = (int) (GameUtils.screenWidth * .1);
        posY = (int) (GameUtils.screenHeight * .1);
        endX = (int) (GameUtils.screenWidth * .9);
        endY = (int) (GameUtils.screenHeight * .5);
        desiredWidth =  (int) (GameUtils.screenWidth * .8);
        textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(fontSize);
        float w = textPaint.measureText("b");
        lettersPerRow = (int)(desiredWidth/w) ;
        setTextToShow("Bienvenido a Globant, este juego te ayudará a integrarte de una manera mas amigable con globant. "
        +" Platica con los otros personajes en la sala, ellos te daran información necesaria para tí,"
        + " y tu estadia en globant.");
    }

    public void update(long step) {
        if(globalText.length()>0 || currentTextToShow.length() >0) {
            isActive = true;
        }

        if(globalText.length()<1 && currentTextToShow.length()< 1) {
            isActive = false;
            GameScene.mthis.unsetActionButtonsFromDialog();
        }
    }

    public void draw(Canvas canvas) {
        if (!isActive) {
            return;
        }

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        Rect rect = new Rect(posX, posY, endX , endY);
        canvas.drawRect(rect, paint);
        paint.setColor(Color.WHITE);
        rect = new Rect(posX + border, posY + border, endX - border, endY - border);
        canvas.drawRect(rect, paint);

        canvas.drawText(currentTextToShow, posX + (2* border), posY +(2 * border) + fontSize , textPaint);//TODO draw text

    }

    public void selectNextText() {
        if(globalText.length() > 0) {
            int NextIndex = globalText.indexOf("\n") +1;
            currentTextToShow = globalText.substring(0, NextIndex);
            globalText = globalText.substring(NextIndex);
        } else {
            currentTextToShow = "";
            globalText = "";
        }
    }

    public void setTextToShow(String text ){
        String newText = "";

        String[] arraytext =text.split(" ");
        for(String s:arraytext) {
            if(s == null)
                continue;
            newText += s+" ";
            if(newText.length() > lettersPerRow) {
                globalText += newText + "\n";
                newText = "";
            }
        }
        if(globalText.isEmpty()&& !newText.isEmpty()) {
            globalText = newText + "\n";
        }
        selectNextText();
    }

}
