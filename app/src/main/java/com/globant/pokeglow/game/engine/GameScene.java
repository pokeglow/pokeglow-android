package com.globant.pokeglow.game.engine;


import com.globant.pokeglow.util.GameUtils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.ArrayList;

public class GameScene {
    ArrayList<SceneActor> actors;
    ArrayList<SceneButton> buttons;
    Bitmap background;
    PlayerActor player;
    GameDialog gameDialog;
    static GameScene mthis;

    public GameScene() {
        player = new PlayerActor();
        actors = new ArrayList<>();
        buttons = new ArrayList<>();
        actors.add(player);
        buttons.add(new ArrowButton(SceneActor.DIRECTION.DOWN, player));
        buttons.add(new ArrowButton(SceneActor.DIRECTION.UP, player));
        buttons.add(new ArrowButton(SceneActor.DIRECTION.LEFT, player));
        buttons.add(new ArrowButton(SceneActor.DIRECTION.RIGHT, player));
        buttons.add(new ActionButton(ActionButton.ActionButtonType.A));
        buttons.add(new ActionButton(ActionButton.ActionButtonType.B));
        buttons.add(new ProfileButton());
        NPCActor paola = new NPCActor("paola", GameUtils.getJsonInfoFromFile("paola.npc"), new Coordinates(GameUtils.screenWidth - 64, ( GameUtils.screenHeight / 2 ) - 64 ) );
        NPCActor eduardo = new NPCActor("eduardo", GameUtils.getJsonInfoFromFile("eduardo.npc"), new Coordinates( 0, ( GameUtils.screenHeight / 2 ) - 64 ) );
        NPCActor mariana = new NPCActor("mariana", GameUtils.getJsonInfoFromFile("mariana.npc"), new Coordinates( (GameUtils.screenWidth / 2) - 64 , 0 ) );
        actors.add(mariana);
        actors.add(eduardo);
        actors.add(paola);
        player.addMovementListener(mariana);
        player.addMovementListener( eduardo );
        player.addMovementListener( paola );
        gameDialog = new GameDialog();
        gameDialog.isActive = true;
        mthis = this;
    }

    public void loadScene() {
        for (SceneActor actor : actors) {
            actor.initInScene( ( GameUtils.screenWidth / 2 ) - 64, ( GameUtils.screenHeight / 2 ) - 64 );
            actor.load();
        }
        for (SceneButton button : buttons) {
            button.load();
        }
    }

    public void draw(Canvas canvas) {
        canvas.drawColor(Color.WHITE);

        for (SceneActor actor : actors) {
            actor.paint(canvas, new Paint());
        }

        for (SceneButton button : buttons) {
            button.paint(canvas, new Paint());
        }

        gameDialog.draw(canvas);
        //TODO draw screne;
    }

    public void updateScene(long step) {

        for(SceneButton button : buttons) {
            button.update(step);
        }

        gameDialog.update(step);
        if(gameDialog.isActive) {
            for(SceneButton button : buttons) {
                if(button instanceof ActionButton){
                    ((ActionButton) button).setCurrentDialog(gameDialog);
                }
            }
            return;
        }

        for (SceneActor actor : actors) {
            actor.step(step);
        }

        //TODO update buttons
    }

    public void sendTouchEvent(int x, int y, int type) {

        for (SceneActor actor : actors) {
            if (actor.isInCoordinate(x, y)) {
                actor.touchEvent(type);
            }
        }

        for (SceneButton button : buttons) {
            boolean isTouchInsideButton = button.isInCoordinate(x, y);
            if (isTouchInsideButton) {
                switch (type) {
                    case 0:
                    case 2:
                        button.setStatus(SceneButton.STATUS.PRESSED);
                        break;
                    case 1:
                        button.setStatus(SceneButton.STATUS.RELEASED);
                        break;
                }

            } else if (button.mStatus == SceneButton.STATUS.PRESSED) {
                button.setStatus(SceneButton.STATUS.RELEASED);
            }
        }

    }

    public void unsetActionButtonsFromDialog() {
        for(SceneButton button : buttons) {
            if(button instanceof ActionButton){
                ((ActionButton) button).setCurrentDialog(null);
            }
        }
    }

    public void validatePlayerCollision() {
        for(SceneActor actor:actors) {
            if(actor instanceof NPCActor) {
                if(((NPCActor)actor).isNearFrom(player.posX, player.posY, 67)) {//distance hardcoded
                    gameDialog.setTextToShow(((NPCActor) actor).getNextTextToShow());
                }
            }
        }
    }
}
