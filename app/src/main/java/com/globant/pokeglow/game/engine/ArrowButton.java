package com.globant.pokeglow.game.engine;

import com.globant.pokeglow.util.GameUtils;


public class ArrowButton extends SceneButton {
    SceneActor.DIRECTION direction;

    PlayerActor player;

    public ArrowButton(SceneActor.DIRECTION initDirection, PlayerActor initPlayer) {
        direction = initDirection;
        player = initPlayer;
    }

    @Override
    public void initInScene(int x, int y) {
        posX = 100;
        posY = 800;
    }

    @Override
    public void load() {
        switch (direction.getValue()) {
            case 0://Down
                currentBitmap = GameUtils.getBitmapFromAsset("down_arrow.png");
                posX = 100;
                posY = GameUtils.screenHeight - 120;
                break;
            case 1://Left
                currentBitmap = GameUtils.getBitmapFromAsset("left_arrow.png");
                posX = 20;
                posY = GameUtils.screenHeight - 200;
                break;
            case 2://Right
                currentBitmap = GameUtils.getBitmapFromAsset("right_arrow.png");
                posX = 180;
                posY = GameUtils.screenHeight - 200;
                break;
            case 3://up
                currentBitmap = GameUtils.getBitmapFromAsset("up_arrow.png");
                posX = 100;
                posY = GameUtils.screenHeight - 280;
                break;
        }
    }

    @Override
    public boolean isInCoordinate(int x, int y) {
        return posX<x && x<(posX+96) && posY <y && y<(posY+96);
    }

    @Override
    public void setStatus(SceneButton.STATUS status) {
        super.setStatus(status);
        player.control(direction, status == STATUS.PRESSED);
    }

    @Override
    public void update(long step) {
        //TODO update button
    }
}
