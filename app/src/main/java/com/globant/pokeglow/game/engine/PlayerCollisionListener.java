package com.globant.pokeglow.game.engine;


/**
 * Created by erick.celis on 21/04/2016.
 */
public interface PlayerCollisionListener {

    boolean playerCollisionedWhileMoving(PlayerActor player);

}
