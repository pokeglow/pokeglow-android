package com.globant.pokeglow.game.engine;

import com.globant.pokeglow.util.GameUtils;


/**
 * Created by erick.celis on 19/04/2016.
 */
public class ActionButton extends SceneButton {

    public enum ActionButtonType {A, B}

    private ActionButtonType buttonType;

    GameDialog currentDialog;

    public ActionButton(ActionButtonType buttonType) {
        this.buttonType = buttonType;
    }

    @Override
    public void initInScene(int x, int y) {
    }

    @Override
    public void load() {
        posY = GameUtils.screenHeight - 200;
        switch (buttonType) {
            case A:
                currentBitmap = GameUtils.getBitmapFromAsset("button_a.png");
                posX = GameUtils.screenWidth - 200;
                break;
            case B:
                currentBitmap = GameUtils.getBitmapFromAsset("button_b.png");
                posX = GameUtils.screenWidth - 100;
                break;
        }
    }

    @Override
    public boolean isInCoordinate(int x, int y) {
        return posX < x && x < (posX + 96) && posY < y && y < (posY + 96);
    }

    @Override
    public void setStatus(SceneButton.STATUS status) {
        super.setStatus(status);
        if (status == STATUS.RELEASED) {//someone release the button
            if (currentDialog == null) {
                GameScene.mthis.validatePlayerCollision();
            } else {
                currentDialog.selectNextText();
            }
        }
    }

    @Override
    public void update(long step) {
        ///TODO uodpate the button
    }

    public void setCurrentDialog(GameDialog dialog) {
        currentDialog = dialog;
    }

}
