package com.globant.pokeglow.ui;

import com.globant.pokeglow.R;

import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.widget.EditText;


public class GloberActivityFunctionalTest extends ActivityInstrumentationTestCase2<GloberActivity> {

    public GloberActivityFunctionalTest() {
        super(GloberActivity.class);
    }

    @UiThreadTest
    public void testShouldSetAGloberNameInNameEditText() {
        final GloberActivity activity = getActivity();
        final EditText nameTextField = (EditText) activity.findViewById(R.id.nameEditText);

        nameTextField.setText("John Doe");

        assertEquals("John Doe", nameTextField.getText().toString());
    }
}
