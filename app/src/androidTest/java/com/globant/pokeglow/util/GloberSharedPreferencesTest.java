package com.globant.pokeglow.util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.InstrumentationTestCase;


@RunWith(AndroidJUnit4.class)
public class GloberSharedPreferencesTest extends InstrumentationTestCase {

    private GloberSharedPreferences preferences;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());

        final Context context = getInstrumentation().getTargetContext();
        preferences = new GloberSharedPreferences(context);
    }

    @Test
    public void testShouldCreateAGloberSharedPreferences() {
        assertNotNull(preferences);
    }

    @Test
    public void testShouldSaveTheGloberNameInGloberSharedPreferences() {
        preferences.startEditSession().
            putGloberName("John Doe").
            save();

        assertEquals("John Doe", preferences.getGloberName());
    }

    @Test
    public void testShouldReturnEmptyWhenNotSharedPreferenceSaved() {
        preferences.reset();
        assertEquals(Constants.EMPTY, preferences.getEmail());
    }
}
